#!/usr/bin/python

import numpy

print "Verry extended 8 queens"

def tokenize(f):
    for line in f:
        for tok in line.split():
            yield tok

with open('input') as f:
    gen = tokenize(f)
    m = int(next(gen))
    n = int(next(gen))
    kings = int(next(gen))
    queens = int(next(gen))
    bishops = int(next(gen))
    rooks = int(next(gen))
    knights = int(next(gen))

m = 8
n = 8

solcount = 0

print m, n, kings, queens, bishops, rooks, knights

def check_horizontal(board, x, y):
    for tx in range(0, m):
        if board[tx, y] < 0:
            return False
    return True

def check_vertical(board, x, y):
    for ty in range(0, n):
        if board[x, ty] < 0:
            return False
    return True

def diag_start(x, y):
    if x >= y:
        return (x-y, 0)
    else:
        return (0, y - x)

def antidiag_start(m, x, y):
    if x + y < m:
        return (x + y, 0)
    else:
        return (m - 1, x + y - (m - 1))
    
def check_diag(board, x, y):
    (tx, ty) = diag_start(x, y)
    while tx < m and ty < n:
        if board[tx][ty] < 0:
            return False
        tx += 1
        ty += 1
    return True

def check_antidiag(board, x, y):
    (tx, ty) = antidiag_start(m, x, y)
    while tx > 0 and ty < n:
        if board[tx][ty] < 0:
            return False
        tx -= 1
        ty +=1
    return True

def mark_queen(board, x, y, val):
    for tx in range(0, m):
        board[tx][y] = board[tx][y] + val
    for ty in range(0, n):
        board[x][ty] = board[x][ty] + val
    (tx, ty) = diag_start(x, y)
    while tx < m and ty < n:
        board[tx][ty] = board[tx][ty] + val
        tx += 1
        ty += 1
    (tx, ty) = antidiag_start(m, x, y)
    while tx > 0 and ty < n:
        board[tx][ty] = board[tx][ty] + val
        tx -= 1
        ty += 1
    if val == -1:
        board[x][y] = 0
    else:
        board[x][y] = -1

def can_place_queen(board, x, y):
    # Zero - position is both unoccupied and not attacked
    if board[x][y] != 0:
        return False
    good = check_diag(board, x, y)
    good = good and check_antidiag(board, x, y)
    good = good and check_horizontal(board, x, y)
    good = good and check_vertical(board, x, y)
    if not good:
        return False
    # print "Can place queen at", x, y
    return True


def solve_rec(board, count):
    global solcount
    if count <= 0:
        #print "/====="
        #print board[0][0], board[0][1], board[0][2]
        #print board[1][0], board[1][1], board[1][2]
        #print "=====/"
        solcount = solcount + 1
        print solcount
        return
    # print "solve_rec", count
    it = numpy.nditer(board, flags=['multi_index'], op_flags=['readwrite'])
    while not it.finished:
        # print "%d <%d, %d>" % (it[0], it.multi_index[0], it.multi_index[1])
        if can_place_queen(board, it.multi_index[0], it.multi_index[1]):
            mark_queen(board, it.multi_index[0], it.multi_index[1], 1)
            solve_rec(board, count - 1)
            mark_queen(board, it.multi_index[0], it.multi_index[1], -1)
        it.iternext()

board = numpy.zeros((m, n))
solve_rec(board, 8)
print "No of solutions:", solcount
